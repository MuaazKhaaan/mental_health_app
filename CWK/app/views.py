from app import app
from app import db
from datetime import datetime
from .models import Student, Diary
from flask import Flask, render_template, redirect, url_for
from flask_bootstrap import Bootstrap
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField
from wtforms.validators import InputRequired, Length
from flask_sqlalchemy  import SQLAlchemy
from flask_login import LoginManager, UserMixin, login_user, login_required, logout_user, current_user

bootstrap = Bootstrap(app)
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'

# Get the user ID
@login_manager.user_loader
def load_user(stu_id):
    return Student.query.get(int(stu_id))
# Form details
class LoginForm(FlaskForm):
    username = StringField('username', validators=[InputRequired(), Length(min=4, max=15)])
    password = PasswordField('password', validators=[InputRequired(), Length(min=8, max=80)])
    remember = BooleanField('remember me')

class MoodForm(FlaskForm):
    description = StringField('Enter Description:', validators=[InputRequired(), Length(min=4, max=15)])
# Start up website on login page
@app.route('/')
def index():
    return redirect(url_for('login'))
# Route to the page
@app.route('/base')
def base():
    return render_template('base.html')

@app.route('/home')
def home():
    return render_template('home.html')

@app.route('/chat')
@login_required # User can only access page is they are logged in
def livechat():
    return render_template('livechat.html')

@app.route('/distractions')
@login_required
def distractions():
    return render_template('distractions.html')

@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    # If login succeeds go to live chat page else failed login page
    if form.validate_on_submit():
        user = Student.query.filter_by(username=form.username.data).first()
        if user:
            if user.password == form.password.data:
                login_user(user, remember=form.remember.data)
                return redirect(url_for('livechat'))

        return render_template('loginfailed.html', form=form)

    return render_template('login.html', form=form)

@app.route('/logout')
@login_required
def logout():
    logout_user()
    return render_template('logout.html')

@app.route('/about')
@login_required
def about():
    return render_template('about.html')

@app.route('/diaryoutput')
@login_required
def diaryoutput():
    output = Diary.query.all()
    return render_template('diaryoutput.html', output=output)


@app.route('/diary', methods=['GET', 'POST'])
@login_required
def diary():
    form = MoodForm()
    return render_template('diary.html', form=form)

@app.route('/new', methods=['POST'])
@login_required
def new():
    form = MoodForm()
    user = current_user.get_id()
    addmood = Diary(description = form.description.data, creator = user, date = datetime.now())
    db.session.add(addmood)
    db.session.commit()
    return redirect(url_for('diaryoutput'))
