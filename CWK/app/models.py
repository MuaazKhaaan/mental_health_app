from app import db
from flask_login import LoginManager, UserMixin, login_user, login_required, logout_user, current_user

class Student(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(15), unique=True)
    email = db.Column(db.String(50), unique=True)
    password = db.Column(db.String(80))
    idlink = db.relationship('Diary', backref='link', lazy='dynamic')
    def __repr__(self):
            return '' % (self.id, self.username, self.email, self.password)

class Diary(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    description = db.Column(db.String(50))
    creator = db.Column(db.Integer, db.ForeignKey('student.id'))
    date = db.Column(db.DateTime)
    def __repr__(self):
            return '' % (self.id, self.description, self.creator)
